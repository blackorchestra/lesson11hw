//
//  CarsCell.swift
//  Cars Shop
//
//  Created by ihor on 21.05.2018.
//  Copyright © 2018 blackorchestra.team. All rights reserved.
//

import Foundation
import UIKit

class CarsCellDetails: UITableViewCell {
    
    @IBOutlet weak var carImg: UIImageView!
    @IBOutlet weak var cost: UILabel!
    @IBOutlet weak var url: UILabel!
    
}
