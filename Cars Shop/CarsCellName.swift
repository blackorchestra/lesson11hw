//
//  CarsCell.swift
//  Cars Shop
//
//  Created by ihor on 21.05.2018.
//  Copyright © 2018 blackorchestra.team. All rights reserved.
//

import Foundation
import UIKit

class CarsCellName: UITableViewCell {
    
    @IBOutlet weak var carImg: UIImageView!
    @IBOutlet weak var carName: UILabel!
      
}
