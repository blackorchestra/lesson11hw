//
//  ViewController.swift
//  Cars Shop
//
//  Created by ihor on 21.05.2018.
//  Copyright © 2018 blackorchestra.team. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var cars: [(name: String, cost: String, link: String, image: UIImage)] = [
        (name: "BMW X2", cost: "19.999$", link: "https://www.bmw.ua/ru/all-models/x-series/X2/2017/bmw-x2.html", image: UIImage(named: "bmwx2")!),
        (name: "BMW M2 Cabriolet", cost: "29.999$", link: "https://www.bmw.ua/ru/all-models/2-series/convertible/2014/at-a-glance.html", image: UIImage(named: "m2cab")!),
        (name: "BMW M140i", cost: "9.999$", link: "https://www.bmw.ua/ru/all-models/1-series/5-door/2015/bmw-mperformance.html", image: UIImage(named: "m140i")!),
        (name: "BMW M2 Coupe", cost: "39.999$", link: "https://www.bmw.ua/ru/all-models/m-series/m2-coupe/2015/at-a-glance.html", image: UIImage(named: "m2coupe")!),
        (name: "BMW M5 Sedan", cost: "29.999$", link: "https://www.bmw.ua/ru/all-models/5-series/sedan/2016/at-a-glance.html", image: UIImage(named: "sedan5series")!)
    ]
    
    var state: Bool = true
    
    @IBOutlet weak var tab: UITableView!
    
    @IBAction func changeBtnClick(_ sender: UIButton) {
        state = !state
        tab.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cars.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: UITableViewCell
        if state {
            let cellName = tableView.dequeueReusableCell(withIdentifier: "carName", for: indexPath) as! CarsCellName
            cellName.carName?.text = cars[indexPath.row].name
            cellName.carImg?.image = cars[indexPath.row].image
            
            cell = cellName
        } else {
            let cellSaleDetails = tableView.dequeueReusableCell(withIdentifier: "carCost", for: indexPath) as! CarsCellDetails
            cellSaleDetails.cost?.text = cars[indexPath.row].cost
            cellSaleDetails.url?.text = cars[indexPath.row].link
            cellSaleDetails.carImg?.image = cars[indexPath.row].image
            
            cell = cellSaleDetails
        }
        
        return cell

    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !state {
            tableView.deselectRow(at: indexPath, animated: true)
            let url = URL(string: cars[indexPath.row].link)!
            UIApplication.shared.open(url)
        }
    }


    override func viewDidLoad() {
        super.viewDidLoad()

    }

}

